﻿using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;

namespace TechTalkDemo.WebApi.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddSwaggerDocs(this IServiceCollection services)
        {
            services.AddSwaggerGen(options =>
            {
                var serviceProvider = services.BuildServiceProvider();
                    options.SwaggerDoc("1.0", new OpenApiInfo
                    {
                        Title = $"Tech Talk Demo API"
                    });
            });
            return services;
        }
    }
}
