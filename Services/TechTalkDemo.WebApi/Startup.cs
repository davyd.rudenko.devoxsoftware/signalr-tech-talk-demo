using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using System.Reflection;
using TechTalkDemo.BusinessLogic.Donations;
using TechTalkDemo.DataAccess;
using TechTalkDemo.DataAccess.Extensions;
using TechTalkDemo.WebApi.Extensions;
using TechTalkDemo.WebApi.Hubs;

namespace TechTalkDemo.WebApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy", builder => builder
                .WithOrigins("http://localhost:4200")
                .AllowAnyMethod()
                .AllowAnyHeader()
                .AllowCredentials());
            });
            var mediatrAssembly = typeof(GetDonations).GetTypeInfo().Assembly;
            services.AddMediatR(mediatrAssembly);
            services.AddSignalR();
            services.AddSwaggerDocs();
            services.AddDbContext<DemoDbContext>(options =>
                options.UseSqlServer(Configuration["Db:ConnectionString"]));
        }
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseCustomSwagger();

            app.UseCors("CorsPolicy");

            app.SeedDatabase(Configuration);

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapHub<DonationsHub>("/donationsHub");
            });
        }
    }
}
