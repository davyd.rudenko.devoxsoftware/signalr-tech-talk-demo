﻿using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using TechTalkDemo.BusinessLogic.People;
using TechTalkDemo.BusinessLogic.People.Models;

namespace TechTalkDemo.WebApi.Controllers
{
    public class PeopleController : ApiController
    {
        public PeopleController(IMediator mediator) : base(mediator)
        {
        }

        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(PersonViewModel))]
        public async Task<ActionResult> Get(int id)
        {
            return Ok(await Mediator.Send(new GetPersonById { PersonId = id }));
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(PeopleListViewModel))]
        public async Task<ActionResult> GetPeople()
        {
            return Ok(await Mediator.Send(new GetPeople()));
        }

    }
}
