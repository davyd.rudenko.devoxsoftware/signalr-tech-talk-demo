﻿using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using System.Threading.Tasks;
using TechTalkDemo.BusinessLogic.Donations;
using TechTalkDemo.BusinessLogic.Donations.Models;
using TechTalkDemo.WebApi.Hubs;

namespace TechTalkDemo.WebApi.Controllers
{
    public class DonationsController : ApiController
    {
        private readonly IHubContext<DonationsHub> hub;
        public DonationsController(IMediator mediator, IHubContext<DonationsHub> hub) : base(mediator)
        {
            this.hub = hub;
        }

        [HttpGet()]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(DonationListViewModel))]
        public async Task<ActionResult> GetDonations()
        {
            return Ok(await Mediator.Send(new GetDonations()));
        }

        [HttpPost()]
        [ProducesResponseType(StatusCodes.Status200OK, Type =typeof(DonationViewModel))]
        public async Task<ActionResult> CreateNewDonation([FromBody] CreateDonation request)
        {
            var result = await Mediator.Send(request);
            await hub.Clients.All.SendAsync("handleNewDonation", result);
            return Ok(result);
        }
    }
}
