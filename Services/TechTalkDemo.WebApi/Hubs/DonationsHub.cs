﻿using Microsoft.AspNetCore.SignalR;
using System.Threading.Tasks;
using TechTalkDemo.BusinessLogic.Donations.Models;

namespace TechTalkDemo.WebApi.Hubs
{
    public class DonationsHub : Hub
    {
        public async Task SendNewDonation(DonationViewModel donation)
        {
            await Clients.All.SendAsync("HandleNewDonation", donation);
        }
    }
}
