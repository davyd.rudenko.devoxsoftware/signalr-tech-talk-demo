﻿using Microsoft.EntityFrameworkCore;
using TechTalkDemo.DataAccess.Models;

namespace TechTalkDemo.DataAccess
{
    public class DemoDbContext : DbContext
    {
        public DbSet<Person> People { get; set; }
        public DbSet<Donation> Donations { get; set; }
        public DemoDbContext()
        {

        }
        public DemoDbContext(DbContextOptions<DemoDbContext> options): base(options)
        {

        }
    }
}
