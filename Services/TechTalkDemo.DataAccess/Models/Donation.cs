﻿using System.ComponentModel.DataAnnotations.Schema;

namespace TechTalkDemo.DataAccess.Models
{
    [Table("Donations")]
    public class Donation
    {
        public int Id { get; set; }
        public decimal Amount { get; set; }
        public int PersonId { get; set; }
        public Person Person { get; set; }
    }
}
