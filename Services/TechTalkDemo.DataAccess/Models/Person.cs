﻿using System.ComponentModel.DataAnnotations.Schema;

namespace TechTalkDemo.DataAccess.Models
{
    [Table("People")]
    public class Person
    {
        public int Id { get; set; }
        public string ProfilePic { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
