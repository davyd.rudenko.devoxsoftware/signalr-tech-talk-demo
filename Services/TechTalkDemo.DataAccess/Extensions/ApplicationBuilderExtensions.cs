﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace TechTalkDemo.DataAccess.Extensions
{
    public static class ApplicationBuilderExtensions
    {
        public static IApplicationBuilder SeedDatabase(this IApplicationBuilder app, Microsoft.Extensions.Configuration.IConfiguration configuration)
        {
            if (configuration == null)
            {
                throw new ArgumentNullException(nameof(configuration).ToString());
            }

            if (bool.TryParse(configuration["Db:Seeding:Enable"], out var seed) && seed)
            {
                if (app == null)
                {
                    throw new ArgumentNullException(nameof(app).ToString());
                }

                using (var serviceScope =
                    app.ApplicationServices.GetRequiredService<IServiceScopeFactory>().CreateScope())
                {
                    var context = serviceScope.ServiceProvider.GetService<DemoDbContext>();
                    context.Seed();
                }
            }

            return app;
        }
    }
}
