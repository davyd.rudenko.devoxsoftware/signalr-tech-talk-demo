﻿using System.Linq;
using TechTalkDemo.DataAccess.Models;

namespace TechTalkDemo.DataAccess.Extensions
{
    public static class DemoDbContextExtensions
    {
        public static void Seed(this DemoDbContext context)
        {
            if (!context.People.Any())
            {
                context.People.AddRange(new Person
                {
                    FirstName = "John",
                    LastName = "Doe",
                    ProfilePic = "https://cutt.ly/xjYwuNz"
                }, new Person
                {
                    FirstName = "Jane",
                    LastName = "Doe",
                    ProfilePic = "https://cutt.ly/xjYwuNz"
                },
                new Person
                {
                    FirstName = "Mike",
                    LastName = "Smith",
                    ProfilePic = "https://cutt.ly/xjYwuNz"
                },
                new Person
                {
                    FirstName = "Alexander",
                    LastName = "The Great",
                    ProfilePic = "https://cutt.ly/xjYwuNz"
                });
                context.SaveChanges();
            }
            if (!context.Donations.Any())
            {
                context.Donations.AddRange(
                    new Donation
                    {
                        PersonId = 1,
                        Amount = 200
                    }, new Donation
                    {
                        PersonId = 1,
                        Amount = 50
                    },
                    new Donation
                    {
                        PersonId = 2,
                        Amount = 100
                    }, new Donation { 
                        PersonId = 3,
                        Amount = 50
                    }, 
                    new Donation { 
                        PersonId = 4,
                        Amount = 400
                    });
                context.SaveChanges();
            }
        }
    }
}
