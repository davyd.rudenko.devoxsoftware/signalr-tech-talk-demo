﻿using System.Collections.Generic;

namespace TechTalkDemo.BusinessLogic.People.Models
{
    public class PeopleListViewModel
    {
        public IEnumerable<PersonViewModel> Collection { get; set; }
    }
}
