﻿using MediatR;
using System.Threading;
using System.Threading.Tasks;
using TechTalkDemo.BusinessLogic.People.Models;
using TechTalkDemo.DataAccess;

namespace TechTalkDemo.BusinessLogic.People
{
    public class GetPersonById : IRequest<PersonViewModel>
    {
        public int PersonId { get; set; }
        public class Handler : IRequestHandler<GetPersonById, PersonViewModel>
        {
            private readonly DemoDbContext dbContext;

            public Handler(DemoDbContext dbContext)
            {
                this.dbContext = dbContext;
            }

            public async Task<PersonViewModel> Handle(GetPersonById request, CancellationToken cancellationToken)
            {
                var person = await dbContext.People.FindAsync(new object[] { request.PersonId });
                return new PersonViewModel
                {
                    Id = person.Id,
                    FirstName = person.FirstName,
                    LastName = person.LastName,
                    ProfilePic = person.ProfilePic
                };
            }
        }
    }
}
