﻿using MediatR;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using TechTalkDemo.BusinessLogic.People.Models;
using TechTalkDemo.DataAccess;

namespace TechTalkDemo.BusinessLogic.People
{
    public class GetPeople : IRequest<PeopleListViewModel>
    {
        public class Handler : IRequestHandler<GetPeople, PeopleListViewModel>
        {
            private readonly DemoDbContext dbContext;

            public Handler(DemoDbContext dbContext)
            {
                this.dbContext = dbContext;
            }

            public Task<PeopleListViewModel> Handle(GetPeople request, CancellationToken cancellationToken)
            {
                var allPeople = dbContext.People.Select(x => new PersonViewModel
                {
                    Id = x.Id,
                    FirstName = x.FirstName,
                    LastName = x.LastName,
                    ProfilePic = x.ProfilePic
                });
                return Task.FromResult(new PeopleListViewModel { Collection = allPeople });
            }
        }
    }
}
