﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TechTalkDemo.BusinessLogic.Donations.Models;
using TechTalkDemo.DataAccess;

namespace TechTalkDemo.BusinessLogic.Donations
{
    public class GetDonations : IRequest<DonationListViewModel>
    {
        public class Handler : IRequestHandler<GetDonations, DonationListViewModel>
        {
            private readonly DemoDbContext dbContext;

            public Handler(DemoDbContext dbContext)
            {
                this.dbContext = dbContext;
            }

            public Task<DonationListViewModel> Handle(GetDonations request, CancellationToken cancellationToken)
            {
                var allDonations = dbContext.Donations.AsNoTracking().Select(x => new DonationViewModel
                {
                    Amount = x.Amount,
                    Id = x.Id,
                    PersonId = x.PersonId
                });

                return Task.FromResult(new DonationListViewModel { Collection = allDonations });
            }
        }
    }
}
