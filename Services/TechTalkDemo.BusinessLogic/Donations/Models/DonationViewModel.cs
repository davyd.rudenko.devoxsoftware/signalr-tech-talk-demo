﻿namespace TechTalkDemo.BusinessLogic.Donations.Models
{
    public class DonationViewModel
    {
        public int Id { get; set; }
        public int PersonId { get; set; }
        public decimal Amount { get; set; }
    }
}
