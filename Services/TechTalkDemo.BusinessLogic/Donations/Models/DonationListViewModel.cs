﻿using System.Collections.Generic;

namespace TechTalkDemo.BusinessLogic.Donations.Models
{
    public class DonationListViewModel
    {
        public IEnumerable<DonationViewModel> Collection { get; set; }
    }
}
