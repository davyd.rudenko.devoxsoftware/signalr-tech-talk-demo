﻿using MediatR;
using System.Threading;
using System.Threading.Tasks;
using TechTalkDemo.BusinessLogic.Donations.Models;
using TechTalkDemo.DataAccess;
using TechTalkDemo.DataAccess.Models;

namespace TechTalkDemo.BusinessLogic.Donations
{
    public class CreateDonation : IRequest<DonationViewModel>
    {
        public int PersonId { get; set; }
        public int Amount { get; set; }

        public class Handler : IRequestHandler<CreateDonation, DonationViewModel>
        {
            private readonly DemoDbContext dbContext;
            public Handler(DemoDbContext dbContext)
            {
                this.dbContext = dbContext;
            }

            public async Task<DonationViewModel> Handle(CreateDonation request, CancellationToken cancellationToken)
            {
                var newDonation = new Donation { Amount = request.Amount, PersonId = request.PersonId };
                dbContext.Donations.Add(newDonation);
                await dbContext.SaveChangesAsync(cancellationToken);
                return new DonationViewModel { Id = newDonation.Id, Amount = newDonation.Amount, PersonId = request.PersonId };
            }
        }
    }
}
