export interface CreateDonationRequest{
    personId: number, 
    amount: number
}