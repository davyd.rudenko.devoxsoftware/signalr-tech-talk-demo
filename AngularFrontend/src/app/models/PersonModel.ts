export interface PersonModel{
  id: number,
  firstName?: string,
  lastName?: string,
  profilePic?: string,
  fullName?: string,
}
