export interface DonationModel{
  id: number,
  personId: number,
  amount: number
}
