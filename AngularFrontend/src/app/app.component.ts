import { Component, ViewChild } from '@angular/core';
import { DonationsService } from './services/donations.service';
import { mergeMap, switchMap } from 'rxjs/operators';
import * as _ from 'lodash';
import { BaseChartDirective } from 'ng2-charts';
import { DonationModel } from './models/DonationModel';
import { PeopleService } from './services/people.service';
import { PersonModel } from './models/PersonModel';
import { ResponseCollection } from './models/ResponseCollection';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  
  constructor() 
  {}

}
