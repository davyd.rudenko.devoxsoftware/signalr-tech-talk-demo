import { Component, EventEmitter, OnInit } from '@angular/core';
import { PersonModel } from 'src/app/models/PersonModel';
import { DonationsService } from 'src/app/services/donations.service';
import { PeopleService } from 'src/app/services/people.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  people = new Array<PersonModel>();
  currentUser: PersonModel;
  donationAmount: number = 100;
  userChanged: EventEmitter<number> = new EventEmitter<number>();

  constructor(private peopleService: PeopleService, private donationService: DonationsService) { }

  ngOnInit(): void {
    this.peopleService.getAllPeople().subscribe(response=>{
      this.people = response.collection;
      this.currentUser = this.people[0];
    });
  }

  updateCurrentUser(userId: number) : void{
    this.userChanged.emit(userId);
    this.currentUser = this.people.find((value,index, arr)=>value.id == userId);
  }

  payDonation(donationAmount: string) : void{
    this.donationService.addDonation({personId : this.currentUser.id, amount : parseFloat(donationAmount)}).subscribe(response=>{
      console.log(response);
    });
  }

}
