import { Component, OnInit, ViewChild } from '@angular/core';
import * as _ from 'lodash';
import { BaseChartDirective } from 'ng2-charts';
import { mergeMap } from 'rxjs/operators';
import { DonationModel } from 'src/app/models/DonationModel';
import { PersonModel } from 'src/app/models/PersonModel';
import { DonationsService } from 'src/app/services/donations.service';
import { PeopleService } from 'src/app/services/people.service';

@Component({
  selector: 'app-donations',
  templateUrl: './donations.component.html',
  styleUrls: ['./donations.component.css']
})
export class DonationsComponent implements OnInit {

  public donations = new Array<number>();
  public people = new Array<PersonModel>();
  public labels = new Array<string>();
  private backgroundColors = new Array<string>();
  public datasets =[{
    backgroundColor: this.backgroundColors,
  }];

  private allPeople = new Array<PersonModel>();

  public chartType = 'pie';

  @ViewChild(BaseChartDirective) chart: BaseChartDirective;

  constructor(public donationsService: DonationsService, private peopleService: PeopleService) {}

  ngOnInit() {
    this.peopleService.getAllPeople().pipe(mergeMap((people)=>{
      this.allPeople = people.collection;
      return this.donationsService.getAllDonations();
    })).subscribe((x) => {
      let donations = x.collection;

      const groupedByPersonId = _(donations).groupBy('personId').map((donations, id) => ({
          personId: id,
          sum: _.sumBy(donations, 'amount'),
        }))
        .value();
      
      console.log(this.allPeople);

      this.people = [...groupedByPersonId].map((value, _index, _arr) => {
        let person = _.find(this.allPeople, (person)=>person.id == +value.personId);
        this.labels[_index] = person.firstName + ' ' + person.lastName;
        return {
          id: person.id, 
          fullName: person.firstName + ' ' + person.lastName,
      };
    });

      this.donations = [...groupedByPersonId].map((value, _index, _arr) => value.sum);

      this.generateBackgroundColors();

    });
    this.donationsService.startConnection();
    this.donationsService.addDonationsDataListener();
    
    this.donationsService.newDonations.subscribe((donation: DonationModel) => {
      let personIndex = _.findIndex(this.people, (person=>person.id == donation.personId));

      this.donations[personIndex] += donation.amount;
      this.labels[personIndex] = this.people[personIndex].fullName;

      this.chart.chart.update();
    });
  }

  generateBackgroundColors():void{
    let i = 0;
    while(i <= this.donations.length) {
      var randomR = Math.floor((Math.random() * 130) + 100);
      var randomG = Math.floor((Math.random() * 130) + 100);
      var randomB = Math.floor((Math.random() * 130) + 100);
    
      var graphBackground = "rgb(" 
              + randomR + ", " 
              + randomG + ", " 
              + randomB + ")";
      this.backgroundColors.push(graphBackground);
      i++;
    }
  }
}
