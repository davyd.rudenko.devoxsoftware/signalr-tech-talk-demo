import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { DonationModel } from '../models/DonationModel';
import * as signalR from '@aspnet/signalr';
import { HttpClient } from '@angular/common/http';
import { ResponseCollection } from '../models/ResponseCollection';
import { CreateDonationRequest } from '../models/CreateDonationRequest';
@Injectable({
  providedIn: 'root'
})
export class DonationsService {
  public newDonations = new Subject<DonationModel>();
  private hubConnection: signalR.HubConnection;

  constructor(private http: HttpClient) { }
  
  public startConnection = () => {
    this.hubConnection = new signalR.HubConnectionBuilder().withUrl('https://localhost:44333/donationsHub').build();
    this.hubConnection.start().then(() => console.log('Connection started')).catch(err => console.log('Error while starting connection: ' + err));
  }

  public addDonationsDataListener = () => {
    this.hubConnection.on('handleNewDonation', (data : DonationModel) => {
      this.newDonations.next(data);
      console.log(data);
    });
  }

  public getAllDonations(): Observable<ResponseCollection<DonationModel>> {
    return this.http.get<ResponseCollection<DonationModel>>("https://localhost:44333/api/donations");
  }

  public addDonation(donation: CreateDonationRequest): Observable<DonationModel> {
    return this.http.post<DonationModel>("https://localhost:44333/api/donations", donation);
  }
}
