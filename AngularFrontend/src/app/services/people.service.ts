import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { PersonModel } from '../models/PersonModel';
import { ResponseCollection } from '../models/ResponseCollection';

@Injectable({
  providedIn: 'root'
})
export class PeopleService {

  constructor(private http: HttpClient) { }

  public getAllPeople(): Observable<ResponseCollection<PersonModel>> {
    return this.http.get<ResponseCollection<PersonModel>>("https://localhost:44333/api/people");
  }
}
